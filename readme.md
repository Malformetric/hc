remove lines from your hosts file if they contain a certain string.    
defaults to your system host file and the string "mojang" as it was designed to clean up after pirated minecraft clients.  

two flags are available:
* -hf  
change the hosts file location, eg -hf="/my_dir" to look for "/my_dir/hosts"
* -rs  
change the string that is removed, eg -rs="my string" to remove lines containing "my string"

planned changes:
* backup the hosts file before editing it, incase the program crashes part way

**Binaries can be found in the bin folder, at this time only the linux binary is tested**  
