package main

import (
	"strings"
	"os"
	"bufio"
	"flag"
	"runtime"
	"fmt"
	"net/http"
)

func main() {
	path := flag.String("hf", "", "path to your hosts file, defaults vary depending on your OS")
	keyword := flag.String("rs", "mojang", "keyword to determine if a line should be removed, defaults to 'mojang'")
	flag.Parse()

	var hint string


	if *path == "" {
		switch runtime.GOOS {
		case "linux":
			*path = "/etc/"
			hint = "try running 'sudo !!'"
		case "darwin":
			*path = "/private/etc/"
			hint = "try running 'sudo !!'"
		case "windows":
			*path = "c:/windows/system32/drivers/etc/"
			hint = "try running this program as administrator"
		default:
			fmt.Println("your platform is not compatible")
			os.Exit(3)
		}
	}

	oldFile, err := os.Open(*path + "hosts")
	if err != nil {
		fmt.Printf("error opening hosts file\n", err)
		os.Exit(1)
	}
	fmt.Println("successfully opened hosts file..")

	scanner := bufio.NewScanner(oldFile)
	var keep []string
	var redirects int
	for scanner.Scan() {
		text := scanner.Text()
		if !strings.Contains(text, *keyword) {
			keep = append(keep, text)
		} else {
			redirects ++
		}
	}
	oldFile.Close()

	if redirects > 0 {
		fmt.Printf("found %v redirects..\n", redirects)
		newFile, err := os.Create(*path + "hosts")
		if err != nil {
			fmt.Printf("error replacing hosts file, do you have permission?\nif not, %v\n%v\n", hint, err)
			os.Exit(1)
		}

		for _, host := range(keep) {
			newFile.WriteString(host + "\n")
		}
		fmt.Println("successfully replaced hosts file..")
		if *keyword == "mojang" {
			fmt.Println("NOTE: if you can now login, change your password or your account may be stolen!")
		}
		newFile.Close()
	} else {
		fmt.Println("found no redirects..")
	}

	address := "http://authserver.mojang.com/"

	resp, err := http.Get("http://authserver.mojang.com/")

	if err != nil {
		fmt.Printf("unable to connect with %v\n%v", address, err)
	} else {
		fmt.Printf("able to connect with %v\n%v    %v", address, resp.StatusCode, resp.Status)
		resp.Body.Close()
	}

	fmt.Println("\ndone. thanks.")

	if runtime.GOOS == "windows" {
		fmt.Scanln()
	}
}